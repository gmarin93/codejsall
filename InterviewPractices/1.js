//Create a function that get a text with words separated by - _ or spaces
// and converts the string to camel case

function toCamelCase(text) {
  
    let wordToArray=[...text];
    let textResult = '';
     
   for(let i=0; i<wordToArray.length;i++){    
     let e = wordToArray[i];
     
     if(e ===" " || e==="-" || e==="_"){
       e=wordToArray[i+1].toUpperCase();
       i++;
     }   
     textResult+= e;
   };
    
   return textResult;  
  }
  
  
  //console.log(toCamelCase('hello_world_1')); // Expected Result: helloWorld1
  console.log(toCamelCase('hello earth world_1')); // Expected Result: helloWorld1
  //console.log(toCamelCase('hello world 1')); // Expected Result: helloWorld1

  //true+true+true = 0;
  //[] == 0 true
  // var a = b = 5 b es igual a number, al igual que a;
